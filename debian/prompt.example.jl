# This example changes the default prompt string.
# Just include this file in ~/.julia/config/startup.jl
# For more tweaks to your REPL, please install the "OhMyREPL" package.
import REPL
function myrepl(repl)
    repl.interface = REPL.setup_interface(repl)
    repl.interface.modes[1].prompt = "⛬ >"
    return
end
atreplinit(myrepl)
