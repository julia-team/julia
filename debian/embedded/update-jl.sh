#!/bin/sh -e
# run this script under debian/embedded/

get_stdlib () {
	prefix=${1}
	url=${2}
	sha1=${3}
	fname=${prefix}-${sha1}
	if ! test -f ${fname}; then
		wget -c ${url}/${sha1} -O ${fname}
		echo debian/embedded/${fname} >> ../source/include-binaries
	fi
}

PKG_SHA1=0fae7809dbaa400d99cbe3a5b39c82332a1381d1
PKG_TAR_URL=https://api.github.com/repos/JuliaLang/Pkg.jl/tarball/
get_stdlib Pkg.jl ${PKG_TAR_URL} ${PKG_SHA1}

STA_SHA1=5256d570d0a554780ed80949c79116f47eac6382
STA_TAR_URL=https://api.github.com/repos/JuliaLang/Statistics.jl/tarball/
get_stdlib Statistics.jl ${STA_TAR_URL} ${STA_SHA1}

LIBCURL_SHA1=8310487053915d5c995513f569ad85ba65c3544f
LIBCURL_TAR_URL=https://api.github.com/repos/JuliaWeb/LibCURL.jl/tarball/
get_stdlib LibCURL.jl ${LIBCURL_TAR_URL} ${LIBCURL_SHA1}

DOWNLOADS_SHA1=b0f23d0ed40cf7c5c5896f957d8163a68dfbc805
DOWNLOADS_TAR_URL=https://api.github.com/repos/JuliaLang/Downloads.jl/tarball/
get_stdlib Downloads.jl ${DOWNLOADS_TAR_URL} ${DOWNLOADS_SHA1}

ARGTOOLS_SHA1=fa878696ff2ae4ba7ca9942bf9544556c0d86ce4
ARGTOOLS_TAR_URL=https://api.github.com/repos/JuliaIO/ArgTools.jl/tarball/
get_stdlib ArgTools.jl ${ARGTOOLS_TAR_URL} ${ARGTOOLS_SHA1}

NETWORKOPTIONS_SHA1=a251de1e1c8ce4edc351d0f05233ba7fe7d2c27a
NETWORKOPTIONS_TAR_URL=https://api.github.com/repos/JuliaLang/NetworkOptions.jl/tarball/
get_stdlib NetworkOptions.jl ${NETWORKOPTIONS_TAR_URL} ${NETWORKOPTIONS_SHA1}

TAR_SHA1=ffb3dd5e697eb6690fce9cceb67edb82134f8337
TAR_TAR_URL=https://api.github.com/repos/JuliaIO/Tar.jl/tarball/
get_stdlib Tar.jl ${TAR_TAR_URL} ${TAR_SHA1}

SUITESPARSE_SHA1=b2d965aa8493f5ef25d6c861ce9d06ed02978ccc
SUITESPARSE_TAR_URL=https://api.github.com/repos/JuliaLang/SuiteSparse.jl/tarball/
get_stdlib SuiteSparse.jl ${SUITESPARSE_TAR_URL} ${SUITESPARSE_SHA1}

# other libraries

LIBUV_SHA1=3a63bf71de62c64097989254e4f03212e3bf5fc8
LIBUV_TAR_URL=https://api.github.com/repos/JuliaLang/libuv/tarball/${LIBUV_SHA1}
fname=libuv-${LIBUV_SHA1}.tar.gz
if ! test -f ${fname}; then
	wget -c ${LIBUV_TAR_URL} -O ${fname}
fi

LIBWHICH_BASE=https://api.github.com/repos/vtjnash/libwhich/tarball/
LIBWHICH_HASH=81e9723c0273d78493dc8c8ed570f68d9ce7e89e
if ! test -r libwhich-${LIBWHICH_HASH}.tar.gz; then
	wget -c ${LIBWHICH_BASE}${LIBWHICH_HASH} -O libwhich-${LIBWHICH_HASH}.tar.gz
fi

BLT_SHA1=23de7a09bf354fe6f655c457bab5bf47fdd2486d
BLT_BASE=https://api.github.com/repos/JuliaLinearAlgebra/libblastrampoline/tarball/
if ! test -r libblastrampoline-${BLT_SHA1}.tar.gz; then
	wget -c ${BLT_BASE}${BLT_SHA1} -O libblastrampoline-${BLT_SHA1}.tar.gz
fi

fname=SuiteSparse-5.10.1.tar.gz
if ! test -f ${fname}; then
	wget -c https://github.com/DrTimothyAldenDavis/SuiteSparse/archive/v5.10.1.tar.gz -O ${fname}
fi

